# Screenpresso License Generator

Allows you to generate and activate a copy of [Screenpresso v2.1.12](https://screenpresso.com/) using offline manual activation.

# About

Developer holds no responsibility with what people decide to do with this app. It was developed strictly for demonstration purposes only.
Developed under the following conditions:

- Visual Studio 2022 (17.6.4)
- v4.8.0 .NET Framework
- C# language

# Usage

If you wish to simply use the keygen, head over to the [Releases](https://github.com/Aetherinox/ScreenpressoKeygen/releases) section and download the latest binary as a `zip` or `rar`. The binary release should only contain one file:

- `ScreenpressoKG.exe`

# Build

Download the source files and launch the `.csproj` project file in Visual Studio.

If you decide to modify or re-build my code, you are to not re-distribute. Unlike a lot of keygens, my files are free of malware, and I do not want people taking advantage of a quick solution that you can dump your non-sense malware into and re-distribute.

If you're looking to do a quick credits swap and re-distribute just so you can make a name for yourself; I'd highly recommend you actually learn C# and make something yourself.

# Signed Releases

As of `v1.0.0.0` and onwards, binaries are GPG signed with the key `CB5C4C30CD0D4028`. You can find the key available on most keyservers:

- [pgp.mit.edu](https://pgp.mit.edu/)
- [keyserver.ubuntu.com](keyserver.ubuntu.com)
- [keys.openpgp.org](https://keys.openpgp.org)

Binaries are also signed with a certificate which has the serial number `70575bdfb02b3f1b45a37431bef9a8c9933a4ace`. If you downloaded this elsewhere on the internet and the binary is not signed with that certificate serial number; **IT IS NOT MINE**. You should delete it.

Don't modify these unless you know what you're doing, improperly configured, the Activation and Response will not generate into a valid serial key.

# Virus Scans

Unfortunately, virus scanners such as VirusTotal can report false positives. There's not much control I have over that. These websites will also attempt to detect keygens; sometimes you can get by it, and sometimes you can't.

The only option I'd have is to contact these websites and tell them that it's a false positive, but I'm writing a keygen; I highly doubt they're going to be happy with doing anything.

I scanned this keygen with Windows Defender and it reported that the files are clean. The other scan reports are listed below:

- [VirusTotal](https://www.virustotal.com/gui/file/e88f80e4478733e444a0ac0eb71b882ff55c83f5401f84f77eeda825a87a4918)
- [Jotti](https://virusscan.jotti.org/en-US/filescanjob/lnnmr0o8zg)
- [Dr. Web](https://online192.drweb.com/cache/?i=91eac05dd9ee4d3cbbf03195589cceca)
- [MetaDefender](https://metadefender.opswat.com/results/file/bzIzMDcwOFdwLS00WE9fb1FkNjRqYjFSdjl0/regular/overview)

# Previews

![Main Screen](https://i.imgur.com/sNCXVle.png)
![Block Host Confirmation](https://i.imgur.com/ucWP4Hk.png)
![Generate License Key](https://i.imgur.com/gukUKPU.png)
![Interface + Help Menu](https://i.imgur.com/j2erqaA.png)
![Valid License](https://i.imgur.com/uVSX2CA.png)
![Activation Complete](https://i.imgur.com/vHIuhf8.png)
![Verifying License](https://i.imgur.com/QWIMaNd.png)
